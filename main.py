from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove, InputFile
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.memory import MemoryStorage
import openai
from config import get_bot_token, get_openai_token

storage = MemoryStorage()
bot = Bot(token=get_bot_token())
dp = Dispatcher(bot=bot, storage=storage)
openai.api_key = get_openai_token()


dream_data = ""
messages = []
CHANEEL_ID = '-1001601569351'
NOTSUBMESSAGE = "Для доступа к функционалу бота - подпишитесь на канал!"

def check_sub_chaneel(chat_member):
    if chat_member["status"] != 'left':
        return True
    else:
        return False

class DreamDesc(StatesGroup):
    place = State()
    time = State()
    main_characters = State()
    main_events = State()


async def on_startup(_):
    print("Bot launched!")


@dp.message_handler(text=['Главная'])
@dp.message_handler(commands=['start', 'help'])
async def start_process(msg: types.Message):
    if check_sub_chaneel(await bot.get_chat_member(chat_id=CHANEEL_ID, user_id=msg.from_user.id)):
        messages.clear()
        b_1 = KeyboardButton("О нашем проекте")
        b_2 = KeyboardButton("Рассказать сон")
        b_3 = KeyboardButton("Обратная связь")
        kb = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(b_1).add(b_2).add(b_3)
                
        await msg.answer(f"Добро пожаловать в мир снов! 🌙✨\n\nЯ - *SomniaBot*, Ваш верный гид по интерпретации снов.\n\nРасскажите мне свои сны, и мы их вместе разберем!", reply_markup=kb, parse_mode="Markdown")
    else:
        b_1 = InlineKeyboardButton(text="Подписаться", url="https://t.me/tell_me_about_ai")
        b_2 = InlineKeyboardButton(text="Подписался", callback_data="subchanneldone")
        
        check_sub_menu = InlineKeyboardMarkup(row_width=1)
        check_sub_menu.insert(b_2)
        check_sub_menu.insert(b_1)
            
        await msg.answer(NOTSUBMESSAGE, reply_markup=check_sub_menu)
                
@dp.callback_query_handler(text="subchanneldone")
async def subchanneldone(msg: types.Message):
    await bot.delete_message(msg.from_user.id, msg.message.message_id)
    if check_sub_chaneel(await bot.get_chat_member(chat_id=CHANEEL_ID, user_id=msg.from_user.id)):
        b_1 = KeyboardButton("О нашем проекте")
        b_2 = KeyboardButton("Рассказать сон")
        b_3 = KeyboardButton("Обратная связь")
        kb = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(b_1).add(b_2).add(b_3)
        
        await bot.send_message(msg.from_user.id, f"Добро пожаловать в мир снов! 🌙✨\n\nЯ - *SomniaBot*, Ваш верный гид по интерпретации снов.\n\nРасскажите мне свои сны, и мы их вместе разберем!", reply_markup=kb, parse_mode="Markdown")
    else:
        b_1 = InlineKeyboardButton(text="Подписаться", url="https://t.me/tell_me_about_ai")
        b_2 = InlineKeyboardButton(text="Подписался", callback_data="subchanneldone")
        check_sub_menu = InlineKeyboardMarkup(row_width=1)
        check_sub_menu.insert(b_2)
        check_sub_menu.insert(b_1)
    
        await bot.send_message(msg.from_user.id, NOTSUBMESSAGE, reply_markup=check_sub_menu)


@dp.message_handler(text=['О нашем проекте'])
async def about_our_project(msg: types.Message):
    b = KeyboardButton("Главная")
    kb = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).row(b)
    await msg.answer("""
    🔮 Добро пожаловать в мир магии и тайн!

🌌 «SomniaBot» - твой надежный проводник в мир астрологии и символики сновидений. Он поможет тебе раскрыть сокровенные тайны, разгадать смысл снов и сделать глубокое погружение в себя. Представляем тебе две функции, которыми обладает наш бот: 

💭 Разгадывание снов: Сны - это дверь в загадочный мир подсознания. Бот поможет тебе исследовать их символику, расшифровать скрытые послания и получить понимание того, что происходит внутри тебя. Просто расскажи боту о своем сне, и он поможет тебе разобраться в его скрытых значениях.

🌟 Наш телеграмм-бот - мудрый наставник, который поможет тебе узнать себя глубже и просветить свой путь. Давай начнем это удивительное путешествие вместе! Присоединяйся к нашему боту прямо сейчас и открой ворота к магическому миру астрологии и толкованию снов. ✨

*Иконки:*
🔮 - шар с прозрачным кристаллом, символизирующий магию и предсказания.
🌌 - галактика, напоминающая о неизведанных пространствах и загадках Вселенной.
🌠 - звезда, символизирующая уникальность и потенциал каждого человека.
💭 - облачко со знаком вопроса, символизирующее загадочные и символичные сновидения.
🌟 - сияющая звезда, символизирующая мудрость и просвещение.""", reply_markup=kb)


@dp.message_handler(text=['Обратная связь'])
async def connection_with_us(msg: types.Message):
    ib_1 = InlineKeyboardButton(text="Главная", callback_data="cancel")
    ib_2 = InlineKeyboardButton(text="Написать нам", url="https://t.me/heloooooo_22")
    ikb = InlineKeyboardMarkup(row_width=2)
    ikb.insert(ib_1)
    ikb.insert(ib_2)
    await msg.answer("Мы ценим вашу обратную связь! 😊\n\nМы всегда стремимся улучшить нашего телеграмм-бота, чтобы предоставить вам наилучший опыт использования. Ваше мнение очень важно для нас, поэтому пожалуйста, поделитесь своими мыслями и предложениями.\n\nЕсли у вас возникли вопросы, замечания или проблемы, не стесняйтесь сообщать нам об этом. Мы готовы помочь вам и разрешить любые возникшие трудности. 🤝🛠️💬✨", reply_markup=ikb)

@dp.message_handler(text=["Рассказать сон"])
@dp.message_handler(commands=['dream'])
async def dream_desc(msg: types.Message, state=None):
    await DreamDesc.place.set()
    await msg.answer("🔎 *Опишите место, которое фигурировало в Вашем сне:*", reply_markup=ReplyKeyboardRemove(), parse_mode="Markdown")


@dp.message_handler(state=DreamDesc.place)
async def dream_place(msg: types.Message, state: FSMContext):
    await state.update_data(place=msg.text)
    await DreamDesc.next()
    await msg.answer("🕗 *Хорошо, теперь опишите время суток в Вашем сне:*", parse_mode="Markdown")


@dp.message_handler(state=DreamDesc.time)
async def dream_place(msg: types.Message, state: FSMContext):
    await state.update_data(time=msg.text)
    await DreamDesc.next()
    await msg.answer("👥 *Отлично, расскажите мне о главных героях Вашего сна:*", parse_mode="Markdown")


@dp.message_handler(state=DreamDesc.main_characters)
async def dream_place(msg: types.Message, state: FSMContext):
    await state.update_data(characters=msg.text)
    await DreamDesc.next()
    await msg.answer("👣 *И наконец, опишите мне подбробно события Вашего сна:*", parse_mode="Markdown")


@dp.message_handler(state=DreamDesc.main_events)
async def dream_place(msg: types.Message, state: FSMContext):
    await state.update_data(events=msg.text)
    await msg.answer("*Проверьте, пожалуйста, введенные Вами данные,\nВсе ли в порядке* ❓", parse_mode="Markdown")

    global dream_data
    dream_data = await state.get_data()
    dream_data = list(dream_data.values())
    dream_data = f"Место событий сна: {dream_data[0]},\n\nВремя событий сна: {dream_data[1]},\n\nГлавные герои сна: {dream_data[2]},\n\nСобытия сна: {dream_data[3]}."

    await state.finish()

    ib_first = InlineKeyboardButton(text="✅", callback_data="ok")
    ib_second = InlineKeyboardButton(text="❌", callback_data="cancel")
    ikb = InlineKeyboardMarkup(row_width=2).insert(ib_first).insert(ib_second)

    await msg.answer(dream_data, reply_markup=ikb)


@dp.callback_query_handler(lambda call: call.data == "ok")
async def ok_answer(call: types.CallbackQuery):
    await call.message.answer("♻ Анализ сна.\nПожалуйста, подождите! 🙏")
    info = dream_data.replace("\n","")
    message = {"role": "user", "content": f"Разбери, пожалуйста, мой сон.{info} Что он значит? Опиши развернуто мой сон и дай 3 жизненных совета, исходя из твоего описания."}
    messages.append(message)
     
    response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=messages,
            temperature=0.5,
            max_tokens=1000)
     
    ib_fist = InlineKeyboardButton("Главная", callback_data="cancel")
    ib_second = InlineKeyboardButton("Визуализировать сон", callback_data="visio")
    ikb = InlineKeyboardMarkup(row_width=1).insert(ib_fist).insert(ib_second)

    await call.message.reply(response["choices"][0]["message"]["content"], reply_markup=ikb) 


@dp.callback_query_handler(lambda call: call.data == "cancel")
async def cancek_answer(call: types.CallbackQuery, state: FSMContext):
    b_1 = KeyboardButton("О нашем проекте")
    b_2 = KeyboardButton("Рассказать сон")
    b_3 = KeyboardButton("Обратная связь")
    kb = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(b_1).add(b_2).add(b_3)
    await call.message.answer(f"Добро пожаловать в мир снов! 🌙✨\n\nЯ - *SomniaBot*, Ваш верный гид по интерпретации снов.\n\nРасскажите мне свои сны, и мы их вместе разберем!", reply_markup=kb, parse_mode="Markdown")
    
    
@dp.callback_query_handler(lambda call: call.data == "visio")
async def dream_visio(call: types.CallbackQuery):
    await call.message.answer("♻ Визуализация сна.\nПожалуйста, подождите! 🙏")         
    info = dream_data.replace("\n","")
    response = openai.Image.create(
        prompt=f"Создай на основе описания моего сна картинку в абстрактно - сюреалистичном стиле. {info}",
        n=1,)
    photo = InputFile.from_url(url=response["data"][0]["url"])
   
    await call.message.answer_photo(photo=photo, reply_markup=ReplyKeyboardMarkup(resize_keyboard=True).add(KeyboardButton("Главная")))

@dp.message_handler()
async def default_answer(msg: types.Message):
    ib_1 = InlineKeyboardButton(text="Главная", callback_data="cancel")
    ikb = InlineKeyboardMarkup(row_width=1)
    ikb.insert(ib_1)
    await msg.reply("*К сожалению, я не понимаю Вас* 😔", parse_mode="Markdown", reply_markup=ikb)

if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
